﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace WCKD_The_Virus
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //Application.EnableVisualStyles();

            var p1 = new System.Windows.Media.MediaPlayer();
            string PathSound = Path.GetFullPath(@"..\..\..\Sounds\TheForest.wav");
            p1.Open(new System.Uri(PathSound));
            p1.Play();
            p1.Volume = 0.3;


            //System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
            //string fullPathToSound = @"..\..\..\Sounds\TheForest.wav";
            //playerMusic.SoundLocation = fullPathToSound;
            //playerMusic.Load();
            //playerMusic.Play();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainMenu());
            
        }
    }
}
