﻿namespace WCKD_The_Virus
{
    partial class youWonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(youWonForm));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Score = new System.Windows.Forms.ProgressBar();
            this.TimeLeft = new System.Windows.Forms.ProgressBar();
            this.HealthLeft = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(49, 333);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 120);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickMap);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Transparent;
            this.button2.Location = new System.Drawing.Point(202, 333);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 120);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickExit);
            // 
            // Score
            // 
            this.Score.Location = new System.Drawing.Point(151, 515);
            this.Score.Maximum = 200;
            this.Score.Name = "Score";
            this.Score.Size = new System.Drawing.Size(202, 23);
            this.Score.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Score.TabIndex = 2;
            this.Score.Value = 50;
            // 
            // TimeLeft
            // 
            this.TimeLeft.ForeColor = System.Drawing.Color.Yellow;
            this.TimeLeft.Location = new System.Drawing.Point(151, 544);
            this.TimeLeft.Maximum = 60;
            this.TimeLeft.Name = "TimeLeft";
            this.TimeLeft.Size = new System.Drawing.Size(202, 23);
            this.TimeLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.TimeLeft.TabIndex = 3;
            this.TimeLeft.Value = 40;
            // 
            // HealthLeft
            // 
            this.HealthLeft.ForeColor = System.Drawing.Color.LimeGreen;
            this.HealthLeft.Location = new System.Drawing.Point(151, 573);
            this.HealthLeft.Name = "HealthLeft";
            this.HealthLeft.Size = new System.Drawing.Size(202, 23);
            this.HealthLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.HealthLeft.TabIndex = 4;
            this.HealthLeft.Value = 80;
            // 
            // youWonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WCKD_The_Virus.Properties.Resources.youWon;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(365, 625);
            this.Controls.Add(this.HealthLeft);
            this.Controls.Add(this.TimeLeft);
            this.Controls.Add(this.Score);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "youWonForm";
            this.Text = "YOU WON!";
            this.ResumeLayout(false);

        }

        public void setScore(int score)
        {
            this.Score.Value = score;
        }
        public void setTime(int time)
        {
            this.TimeLeft.Value = time;
        }

        public void setHealth(int health)
        {
            this.HealthLeft.Value = health;
        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ProgressBar Score;
        private System.Windows.Forms.ProgressBar TimeLeft;
        private System.Windows.Forms.ProgressBar HealthLeft;
    }
}