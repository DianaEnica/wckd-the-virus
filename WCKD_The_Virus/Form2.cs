﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WCKD_The_Virus
{
    public partial class Form2 : Form
    {
        int timeLeft = 1;
        int lvlComplete = 0;
        List<int> visited = new List<int>(40);

        public void setVisited()
        {
            for (int i = 0; i <= 27; i++)
            {
                visited.Insert(i, 0);
            }
        }

        public Form2()
        {
            InitializeComponent();
            setVisited();
            int health = 100;
            timer.Start();
            timer.Interval = 1000;
            MotherBoard2.Controls.Add(WCKD);
            WCKD.BackColor = Color.Transparent;
            this.KeyDown += new KeyEventHandler(this.Game_KeyDown);
            this.KeyDown += new KeyEventHandler(this.Collisions);
            #region visibilityBoxes
            BoxASUS1.Visible = false;
            BoxASUS2.Visible = false;
            BoxASUS3.Visible = false;
            BoxASUS4.Visible = false;
            BoxASUS5.Visible = false;
            BoxASUS6.Visible = false;
            BoxASUS7.Visible = false;
            BoxASUS8.Visible = false;
            BoxASUS9.Visible = false;
            BoxASUS10.Visible = false;
            BoxASUS11.Visible = false;
            BoxASUS12.Visible = false;
            BoxASUS13.Visible = false;
            BoxASUS14.Visible = false;
            BoxASUS15.Visible = false;
            BoxASUS16.Visible = false;
            BoxASUS17.Visible = false;
            BoxASUS20.Visible = false;
            BoxASUS21.Visible = false;
            BoxASUS22.Visible = false;
            BoxASUS23.Visible = false;
            BoxASUS24.Visible = false;
            BoxASUS25.Visible = false;
            BoxASUS26.Visible = false;
            BoxASUS27.Visible = false;
            BoxASUS28.Visible = false;
            BoxASUS29.Visible = false;
            BoxASUS30.Visible = false;
            BoxASUS31.Visible = false;
            BoxASUS32.Visible = false;
            BoxASUS33.Visible = false;
            BoxASUS34.Visible = false;
            BoxASUS35.Visible = false;
            BoxASUS36.Visible = false;
            BoxASUS37.Visible = false;
            BoxASUS38.Visible = false;
            #endregion
        }

        private void Game_KeyDown(object sender, KeyEventArgs key)
        {
            if (key.KeyCode == Keys.Up)
            {
                WCKD.Top -= 5;
            }
            else if (key.KeyCode == Keys.Down)
            {
                WCKD.Top += 5;
            }
            else if (key.KeyCode == Keys.Left)
            {
                WCKD.Left -= 5;
            }
            else if (key.KeyCode == Keys.Right)
            {
                WCKD.Left += 5;
            }
            else if (key.KeyCode == Keys.Escape)
            {
                timer.Stop();
                this.Hide();
                Maps open_game_maps = new Maps();
                open_game_maps.Closed += (s, args) => this.Close();
                open_game_maps.Show();
            }
        }

        public void Collisions(object sender, KeyEventArgs key)
        {
            #region boundsIntersection
            if (WCKD.Bounds.IntersectsWith(BorderLeft.Bounds) || WCKD.Bounds.IntersectsWith(BorderRight.Bounds) ||
                WCKD.Bounds.IntersectsWith(BorderTop.Bounds) || WCKD.Bounds.IntersectsWith(BorderBottom.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS1.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS2.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS3.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS4.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS5.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS6.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS7.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS8.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS9.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS10.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS11.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS12.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS13.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS14.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS15.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS16.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS17.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS20.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS21.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS22.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS23.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS24.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS25.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS26.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS27.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS28.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS29.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS30.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS31.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS32.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS33.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS34.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS35.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS36.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxASUS37.Bounds) || WCKD.Bounds.IntersectsWith(BoxASUS38.Bounds)
                )

            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\collision.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                if (Life.Value >= 1)
                {
                    Life.Value -= 5;
                }
                else
                {
                    System.Media.SoundPlayer playerMusic1 = new System.Media.SoundPlayer();
                    string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                    playerMusic.SoundLocation = fullPathToSound1;
                    playerMusic.Load();
                    playerMusic.Play();
                    timer.Stop();
                    this.Hide();
                    GameOverForm2 open_game_form = new GameOverForm2();
                    open_game_form.setScore(Points.Value);
                    open_game_form.setTime(timpRamas.Value);
                    open_game_form.setHealth(Life.Value);
                    open_game_form.Closed += (s, args) => this.Close();
                    open_game_form.Show();
                }

                if (key.KeyCode == Keys.Up)
                {
                    WCKD.Top += 5;

                }
                else if (key.KeyCode == Keys.Down)
                {
                    WCKD.Top -= 5;
                }
                else if (key.KeyCode == Keys.Left)
                {
                    WCKD.Left += 5;
                }
                else if (key.KeyCode == Keys.Right)
                {
                    WCKD.Left -= 5;
                }
            }
            #endregion
            #region pointsIntersection
            else if (WCKD.Bounds.IntersectsWith(p1.Bounds))
            {
                if (visited[1] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p1.Visible = false;
                    visited[1]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p2.Bounds))
            {
                if (visited[2] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p2.Visible = false;
                    visited[2]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p3.Bounds))
            {
                if (visited[3] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p3.Visible = false;
                    visited[3]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p4.Bounds))
            {
                if (visited[4] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p4.Visible = false;
                    visited[4]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p5.Bounds))
            {
                if (visited[5] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p5.Visible = false;
                    visited[5]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p6.Bounds))
            {
                if (visited[6] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p6.Visible = false;
                    visited[6]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p7.Bounds))
            {
                if (visited[7] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p7.Visible = false;
                    visited[7]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p8.Bounds))
            {
                if (visited[8] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p8.Visible = false;
                    visited[8]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p9.Bounds))
            {
                if (visited[9] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p9.Visible = false;
                    visited[9]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p10.Bounds))
            {
                if (visited[10] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p10.Visible = false;
                    visited[10]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p11.Bounds))
            {
                if (visited[11] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p11.Visible = false;
                    visited[11]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p12.Bounds))
            {
                if (visited[12] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p12.Visible = false;
                    visited[12]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p13.Bounds))
            {
                if (visited[13] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p13.Visible = false;
                    visited[13]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p14.Bounds))
            {
                if (visited[14] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p14.Visible = false;
                    visited[14]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p15.Bounds))
            {
                if (visited[15] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p15.Visible = false;
                    visited[15]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p16.Bounds))
            {
                if (visited[16] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p16.Visible = false;
                    visited[16]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p17.Bounds))
            {
                if (visited[17] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p17.Visible = false;
                    visited[17]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p18.Bounds))
            {
                if (visited[18] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p18.Visible = false;
                    visited[18]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p19.Bounds))
            {
                if (visited[19] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p19.Visible = false;
                    visited[19]++;
                }

            }
            else if (WCKD.Bounds.IntersectsWith(p20.Bounds))
            {

                if (visited[20] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p20.Visible = false;

                    visited[20]++;
                }


            }
            else if (WCKD.Bounds.IntersectsWith(p21.Bounds))
            {
                if (visited[21] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p21.Visible = false;
                    visited[21]++;
                }



            }
            else if (WCKD.Bounds.IntersectsWith(p22.Bounds))
            {
                if (visited[22] == 00)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p22.Visible = false;
                    visited[22]++;
                }


            }
            else if (WCKD.Bounds.IntersectsWith(p23.Bounds))
            {

                if (visited[23] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p23.Visible = false;
                    visited[23]++;
                }

            }
            else if (WCKD.Bounds.IntersectsWith(p24.Bounds))
            {

                if (visited[24] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p24.Visible = false;
                    visited[24]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p25.Bounds))
            {

                if (visited[25] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p25.Visible = false;
                    visited[25]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p26.Bounds))
            {

                if (visited[26] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p26.Visible = false;
                    visited[26]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p27.Bounds))
            {

                if (visited[27] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 192)
                    {
                        Points.Value += 8;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p27.Visible = false;
                    visited[27]++;
                }
            }
            #endregion
            #region deathIntersection
            else if (WCKD.Bounds.IntersectsWith(Death1.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death2.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death3.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death4.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death5.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death6.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death7.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death8.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death9.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death10.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death11.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death12.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death13.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death14.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death15.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death16.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death17.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death18.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death19.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(Death20.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound1;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            #endregion
            #region finishIntersection
            else if (WCKD.Bounds.IntersectsWith(finish.Bounds) && lvlComplete == 1)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\Shutdown.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                this.Hide();
                YouWonForm2 open_gamew_form = new YouWonForm2();
                open_gamew_form.setScore(Points.Value);
                open_gamew_form.setTime(timpRamas.Value);
                open_gamew_form.setHealth(Life.Value);
                open_gamew_form.Closed += (s, args) => this.Close();
                open_gamew_form.Show();
            }
            #endregion
        }



        private void timer_Tick(object sender, EventArgs e)
        {
            timeLeft += 1;
            if (timeLeft == 60)
            {
                timer.Stop();
                this.Hide();
                GameOverForm2 open_game_form = new GameOverForm2();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else
            {
                timpRamas.Value = 60 - timeLeft;
            }
        }
    }
}
