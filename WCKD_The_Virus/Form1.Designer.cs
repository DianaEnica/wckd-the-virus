﻿using System.Drawing;

namespace WCKD_The_Virus
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Life = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.Points = new System.Windows.Forms.ProgressBar();
            this.PointsText = new System.Windows.Forms.Label();
            this.timpRamas = new System.Windows.Forms.ProgressBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.finish = new System.Windows.Forms.PictureBox();
            this.p25 = new System.Windows.Forms.PictureBox();
            this.e8 = new System.Windows.Forms.PictureBox();
            this.e3 = new System.Windows.Forms.PictureBox();
            this.e4 = new System.Windows.Forms.PictureBox();
            this.e2 = new System.Windows.Forms.PictureBox();
            this.e6 = new System.Windows.Forms.PictureBox();
            this.e5 = new System.Windows.Forms.PictureBox();
            this.e7 = new System.Windows.Forms.PictureBox();
            this.e1 = new System.Windows.Forms.PictureBox();
            this.p23 = new System.Windows.Forms.PictureBox();
            this.p24 = new System.Windows.Forms.PictureBox();
            this.p22 = new System.Windows.Forms.PictureBox();
            this.p20 = new System.Windows.Forms.PictureBox();
            this.p21 = new System.Windows.Forms.PictureBox();
            this.p19 = new System.Windows.Forms.PictureBox();
            this.p18 = new System.Windows.Forms.PictureBox();
            this.p16 = new System.Windows.Forms.PictureBox();
            this.p17 = new System.Windows.Forms.PictureBox();
            this.p14 = new System.Windows.Forms.PictureBox();
            this.p15 = new System.Windows.Forms.PictureBox();
            this.p13 = new System.Windows.Forms.PictureBox();
            this.p12 = new System.Windows.Forms.PictureBox();
            this.p11 = new System.Windows.Forms.PictureBox();
            this.p10 = new System.Windows.Forms.PictureBox();
            this.p9 = new System.Windows.Forms.PictureBox();
            this.p8 = new System.Windows.Forms.PictureBox();
            this.p1 = new System.Windows.Forms.PictureBox();
            this.p2 = new System.Windows.Forms.PictureBox();
            this.p3 = new System.Windows.Forms.PictureBox();
            this.p4 = new System.Windows.Forms.PictureBox();
            this.p5 = new System.Windows.Forms.PictureBox();
            this.p6 = new System.Windows.Forms.PictureBox();
            this.p7 = new System.Windows.Forms.PictureBox();
            this.BoxMSI7 = new System.Windows.Forms.PictureBox();
            this.BoxMSI2 = new System.Windows.Forms.PictureBox();
            this.BoxMSI5 = new System.Windows.Forms.PictureBox();
            this.BoxMSI6 = new System.Windows.Forms.PictureBox();
            this.BoxMSI3 = new System.Windows.Forms.PictureBox();
            this.BoxMSI4 = new System.Windows.Forms.PictureBox();
            this.WCKD = new System.Windows.Forms.PictureBox();
            this.BorderLeft = new System.Windows.Forms.PictureBox();
            this.BorderBottom = new System.Windows.Forms.PictureBox();
            this.BorderRight = new System.Windows.Forms.PictureBox();
            this.BorderTop = new System.Windows.Forms.PictureBox();
            this.BoxPCI1 = new System.Windows.Forms.PictureBox();
            this.BoxTransistor2 = new System.Windows.Forms.PictureBox();
            this.BoxMSI = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.BoxTransistor6 = new System.Windows.Forms.PictureBox();
            this.BoxTransistor5 = new System.Windows.Forms.PictureBox();
            this.BoxPCI5 = new System.Windows.Forms.PictureBox();
            this.BoxPCI3 = new System.Windows.Forms.PictureBox();
            this.BoxTransistor3 = new System.Windows.Forms.PictureBox();
            this.BoxTransistor1 = new System.Windows.Forms.PictureBox();
            this.BoxPCI6 = new System.Windows.Forms.PictureBox();
            this.BoxPCI4 = new System.Windows.Forms.PictureBox();
            this.BoxSSD = new System.Windows.Forms.PictureBox();
            this.BoxPCI2 = new System.Windows.Forms.PictureBox();
            this.BorderLeft3 = new System.Windows.Forms.PictureBox();
            this.BoxCPU = new System.Windows.Forms.PictureBox();
            this.BoxRam = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.MotherBoard1 = new System.Windows.Forms.PictureBox();
            this.MarginBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.finish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxSSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderLeft3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxCPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxRam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotherBoard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MarginBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Life
            // 
            this.Life.ForeColor = System.Drawing.Color.LimeGreen;
            this.Life.Location = new System.Drawing.Point(437, 767);
            this.Life.Name = "Life";
            this.Life.Size = new System.Drawing.Size(194, 28);
            this.Life.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Life.TabIndex = 35;
            this.Life.Value = 100;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LimeGreen;
            this.label2.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(502, 770);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 23);
            this.label2.TabIndex = 36;
            this.label2.Text = "Health";
            this.label2.Visible = false;
            // 
            // Points
            // 
            this.Points.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.Points.Location = new System.Drawing.Point(153, 767);
            this.Points.Maximum = 200;
            this.Points.Name = "Points";
            this.Points.Size = new System.Drawing.Size(278, 28);
            this.Points.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Points.TabIndex = 37;
            // 
            // PointsText
            // 
            this.PointsText.AutoSize = true;
            this.PointsText.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.PointsText.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointsText.Location = new System.Drawing.Point(244, 770);
            this.PointsText.Name = "PointsText";
            this.PointsText.Size = new System.Drawing.Size(99, 23);
            this.PointsText.TabIndex = 38;
            this.PointsText.Text = "PointsToGo";
            this.PointsText.Visible = false;
            // 
            // timpRamas
            // 
            this.timpRamas.ForeColor = System.Drawing.Color.Yellow;
            this.timpRamas.Location = new System.Drawing.Point(25, 767);
            this.timpRamas.Maximum = 60;
            this.timpRamas.Name = "timpRamas";
            this.timpRamas.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.timpRamas.Size = new System.Drawing.Size(122, 28);
            this.timpRamas.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.timpRamas.TabIndex = 39;
            this.timpRamas.Value = 60;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Yellow;
            this.label3.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(42, 770);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 23);
            this.label3.TabIndex = 67;
            this.label3.Text = "Time Left";
            this.label3.Visible = false;
            // 
            // finish
            // 
            this.finish.BackColor = System.Drawing.Color.Red;
            this.finish.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.finish.Location = new System.Drawing.Point(223, 399);
            this.finish.Name = "finish";
            this.finish.Size = new System.Drawing.Size(38, 33);
            this.finish.TabIndex = 77;
            this.finish.TabStop = false;
            this.finish.Visible = false;
            // 
            // p25
            // 
            this.p25.BackColor = System.Drawing.Color.Lime;
            this.p25.Location = new System.Drawing.Point(357, 429);
            this.p25.Name = "p25";
            this.p25.Size = new System.Drawing.Size(10, 10);
            this.p25.TabIndex = 76;
            this.p25.TabStop = false;
            // 
            // e8
            // 
            this.e8.BackColor = System.Drawing.Color.Red;
            this.e8.Location = new System.Drawing.Point(121, 40);
            this.e8.Name = "e8";
            this.e8.Size = new System.Drawing.Size(10, 10);
            this.e8.TabIndex = 75;
            this.e8.TabStop = false;
            this.e8.Visible = false;
            // 
            // e3
            // 
            this.e3.BackColor = System.Drawing.Color.Red;
            this.e3.Location = new System.Drawing.Point(616, 35);
            this.e3.Name = "e3";
            this.e3.Size = new System.Drawing.Size(10, 10);
            this.e3.TabIndex = 74;
            this.e3.TabStop = false;
            this.e3.Visible = false;
            // 
            // e4
            // 
            this.e4.BackColor = System.Drawing.Color.Red;
            this.e4.Location = new System.Drawing.Point(434, 35);
            this.e4.Name = "e4";
            this.e4.Size = new System.Drawing.Size(10, 10);
            this.e4.TabIndex = 73;
            this.e4.TabStop = false;
            this.e4.Visible = false;
            // 
            // e2
            // 
            this.e2.BackColor = System.Drawing.Color.Red;
            this.e2.Location = new System.Drawing.Point(616, 416);
            this.e2.Name = "e2";
            this.e2.Size = new System.Drawing.Size(10, 10);
            this.e2.TabIndex = 72;
            this.e2.TabStop = false;
            this.e2.Visible = false;
            // 
            // e6
            // 
            this.e6.BackColor = System.Drawing.Color.Red;
            this.e6.Location = new System.Drawing.Point(442, 716);
            this.e6.Name = "e6";
            this.e6.Size = new System.Drawing.Size(10, 10);
            this.e6.TabIndex = 71;
            this.e6.TabStop = false;
            this.e6.Visible = false;
            // 
            // e5
            // 
            this.e5.BackColor = System.Drawing.Color.Red;
            this.e5.Location = new System.Drawing.Point(616, 716);
            this.e5.Name = "e5";
            this.e5.Size = new System.Drawing.Size(10, 10);
            this.e5.TabIndex = 70;
            this.e5.TabStop = false;
            this.e5.Visible = false;
            // 
            // e7
            // 
            this.e7.BackColor = System.Drawing.Color.Red;
            this.e7.Location = new System.Drawing.Point(69, 714);
            this.e7.Name = "e7";
            this.e7.Size = new System.Drawing.Size(10, 10);
            this.e7.TabIndex = 69;
            this.e7.TabStop = false;
            this.e7.Visible = false;
            // 
            // e1
            // 
            this.e1.BackColor = System.Drawing.Color.Red;
            this.e1.Location = new System.Drawing.Point(434, 413);
            this.e1.Name = "e1";
            this.e1.Size = new System.Drawing.Size(10, 10);
            this.e1.TabIndex = 68;
            this.e1.TabStop = false;
            this.e1.Visible = false;
            // 
            // p23
            // 
            this.p23.BackColor = System.Drawing.Color.Lime;
            this.p23.Location = new System.Drawing.Point(98, 622);
            this.p23.Name = "p23";
            this.p23.Size = new System.Drawing.Size(10, 10);
            this.p23.TabIndex = 66;
            this.p23.TabStop = false;
            // 
            // p24
            // 
            this.p24.BackColor = System.Drawing.Color.Lime;
            this.p24.Location = new System.Drawing.Point(79, 623);
            this.p24.Name = "p24";
            this.p24.Size = new System.Drawing.Size(10, 10);
            this.p24.TabIndex = 65;
            this.p24.TabStop = false;
            // 
            // p22
            // 
            this.p22.BackColor = System.Drawing.Color.Lime;
            this.p22.Location = new System.Drawing.Point(98, 600);
            this.p22.Name = "p22";
            this.p22.Size = new System.Drawing.Size(10, 10);
            this.p22.TabIndex = 62;
            this.p22.TabStop = false;
            // 
            // p20
            // 
            this.p20.BackColor = System.Drawing.Color.Lime;
            this.p20.Location = new System.Drawing.Point(172, 589);
            this.p20.Name = "p20";
            this.p20.Size = new System.Drawing.Size(10, 10);
            this.p20.TabIndex = 61;
            this.p20.TabStop = false;
            // 
            // p21
            // 
            this.p21.BackColor = System.Drawing.Color.Lime;
            this.p21.Location = new System.Drawing.Point(79, 600);
            this.p21.Name = "p21";
            this.p21.Size = new System.Drawing.Size(10, 10);
            this.p21.TabIndex = 60;
            this.p21.TabStop = false;
            // 
            // p19
            // 
            this.p19.BackColor = System.Drawing.Color.Lime;
            this.p19.Location = new System.Drawing.Point(150, 589);
            this.p19.Name = "p19";
            this.p19.Size = new System.Drawing.Size(10, 10);
            this.p19.TabIndex = 59;
            this.p19.TabStop = false;
            // 
            // p18
            // 
            this.p18.BackColor = System.Drawing.Color.Lime;
            this.p18.Location = new System.Drawing.Point(149, 441);
            this.p18.Name = "p18";
            this.p18.Size = new System.Drawing.Size(10, 10);
            this.p18.TabIndex = 58;
            this.p18.TabStop = false;
            // 
            // p16
            // 
            this.p16.BackColor = System.Drawing.Color.Lime;
            this.p16.Location = new System.Drawing.Point(354, 380);
            this.p16.Name = "p16";
            this.p16.Size = new System.Drawing.Size(10, 10);
            this.p16.TabIndex = 57;
            this.p16.TabStop = false;
            // 
            // p17
            // 
            this.p17.BackColor = System.Drawing.Color.Lime;
            this.p17.Location = new System.Drawing.Point(335, 380);
            this.p17.Name = "p17";
            this.p17.Size = new System.Drawing.Size(10, 10);
            this.p17.TabIndex = 56;
            this.p17.TabStop = false;
            // 
            // p14
            // 
            this.p14.BackColor = System.Drawing.Color.Lime;
            this.p14.Location = new System.Drawing.Point(581, 266);
            this.p14.Name = "p14";
            this.p14.Size = new System.Drawing.Size(10, 10);
            this.p14.TabIndex = 55;
            this.p14.TabStop = false;
            // 
            // p15
            // 
            this.p15.BackColor = System.Drawing.Color.Lime;
            this.p15.Location = new System.Drawing.Point(581, 310);
            this.p15.Name = "p15";
            this.p15.Size = new System.Drawing.Size(10, 10);
            this.p15.TabIndex = 54;
            this.p15.TabStop = false;
            // 
            // p13
            // 
            this.p13.BackColor = System.Drawing.Color.Lime;
            this.p13.Location = new System.Drawing.Point(568, 247);
            this.p13.Name = "p13";
            this.p13.Size = new System.Drawing.Size(10, 10);
            this.p13.TabIndex = 53;
            this.p13.TabStop = false;
            // 
            // p12
            // 
            this.p12.BackColor = System.Drawing.Color.Lime;
            this.p12.Location = new System.Drawing.Point(568, 230);
            this.p12.Name = "p12";
            this.p12.Size = new System.Drawing.Size(10, 10);
            this.p12.TabIndex = 52;
            this.p12.TabStop = false;
            // 
            // p11
            // 
            this.p11.BackColor = System.Drawing.Color.Lime;
            this.p11.Location = new System.Drawing.Point(568, 213);
            this.p11.Name = "p11";
            this.p11.Size = new System.Drawing.Size(10, 10);
            this.p11.TabIndex = 51;
            this.p11.TabStop = false;
            // 
            // p10
            // 
            this.p10.BackColor = System.Drawing.Color.Lime;
            this.p10.Location = new System.Drawing.Point(569, 126);
            this.p10.Name = "p10";
            this.p10.Size = new System.Drawing.Size(10, 10);
            this.p10.TabIndex = 50;
            this.p10.TabStop = false;
            // 
            // p9
            // 
            this.p9.BackColor = System.Drawing.Color.Lime;
            this.p9.Location = new System.Drawing.Point(568, 108);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(10, 10);
            this.p9.TabIndex = 49;
            this.p9.TabStop = false;
            // 
            // p8
            // 
            this.p8.BackColor = System.Drawing.Color.Lime;
            this.p8.Location = new System.Drawing.Point(190, 35);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(13, 15);
            this.p8.TabIndex = 48;
            this.p8.TabStop = false;
            // 
            // p1
            // 
            this.p1.BackColor = System.Drawing.Color.Lime;
            this.p1.Location = new System.Drawing.Point(223, 154);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(10, 10);
            this.p1.TabIndex = 46;
            this.p1.TabStop = false;
            // 
            // p2
            // 
            this.p2.BackColor = System.Drawing.Color.Lime;
            this.p2.Location = new System.Drawing.Point(223, 171);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(10, 10);
            this.p2.TabIndex = 45;
            this.p2.TabStop = false;
            // 
            // p3
            // 
            this.p3.BackColor = System.Drawing.Color.Lime;
            this.p3.Location = new System.Drawing.Point(223, 188);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(10, 10);
            this.p3.TabIndex = 44;
            this.p3.TabStop = false;
            // 
            // p4
            // 
            this.p4.BackColor = System.Drawing.Color.Lime;
            this.p4.Location = new System.Drawing.Point(222, 205);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(10, 10);
            this.p4.TabIndex = 43;
            this.p4.TabStop = false;
            // 
            // p5
            // 
            this.p5.BackColor = System.Drawing.Color.Lime;
            this.p5.Location = new System.Drawing.Point(223, 223);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(10, 10);
            this.p5.TabIndex = 42;
            this.p5.TabStop = false;
            // 
            // p6
            // 
            this.p6.BackColor = System.Drawing.Color.Lime;
            this.p6.Location = new System.Drawing.Point(223, 240);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(10, 10);
            this.p6.TabIndex = 41;
            this.p6.TabStop = false;
            // 
            // p7
            // 
            this.p7.BackColor = System.Drawing.Color.Lime;
            this.p7.Location = new System.Drawing.Point(223, 257);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(10, 10);
            this.p7.TabIndex = 40;
            this.p7.TabStop = false;
            // 
            // BoxMSI7
            // 
            this.BoxMSI7.BackColor = System.Drawing.Color.Red;
            this.BoxMSI7.Location = new System.Drawing.Point(458, 462);
            this.BoxMSI7.Name = "BoxMSI7";
            this.BoxMSI7.Size = new System.Drawing.Size(32, 28);
            this.BoxMSI7.TabIndex = 34;
            this.BoxMSI7.TabStop = false;
            this.BoxMSI7.Visible = false;
            // 
            // BoxMSI2
            // 
            this.BoxMSI2.BackColor = System.Drawing.Color.Red;
            this.BoxMSI2.Location = new System.Drawing.Point(451, 480);
            this.BoxMSI2.Name = "BoxMSI2";
            this.BoxMSI2.Size = new System.Drawing.Size(39, 16);
            this.BoxMSI2.TabIndex = 33;
            this.BoxMSI2.TabStop = false;
            this.BoxMSI2.Visible = false;
            // 
            // BoxMSI5
            // 
            this.BoxMSI5.BackColor = System.Drawing.Color.Red;
            this.BoxMSI5.Location = new System.Drawing.Point(530, 591);
            this.BoxMSI5.Name = "BoxMSI5";
            this.BoxMSI5.Size = new System.Drawing.Size(10, 24);
            this.BoxMSI5.TabIndex = 32;
            this.BoxMSI5.TabStop = false;
            this.BoxMSI5.Visible = false;
            // 
            // BoxMSI6
            // 
            this.BoxMSI6.BackColor = System.Drawing.Color.Red;
            this.BoxMSI6.Location = new System.Drawing.Point(530, 576);
            this.BoxMSI6.Name = "BoxMSI6";
            this.BoxMSI6.Size = new System.Drawing.Size(19, 21);
            this.BoxMSI6.TabIndex = 31;
            this.BoxMSI6.TabStop = false;
            this.BoxMSI6.Visible = false;
            // 
            // BoxMSI3
            // 
            this.BoxMSI3.BackColor = System.Drawing.Color.Red;
            this.BoxMSI3.Location = new System.Drawing.Point(479, 451);
            this.BoxMSI3.Name = "BoxMSI3";
            this.BoxMSI3.Size = new System.Drawing.Size(53, 45);
            this.BoxMSI3.TabIndex = 30;
            this.BoxMSI3.TabStop = false;
            this.BoxMSI3.Visible = false;
            // 
            // BoxMSI4
            // 
            this.BoxMSI4.BackColor = System.Drawing.Color.Red;
            this.BoxMSI4.Location = new System.Drawing.Point(530, 451);
            this.BoxMSI4.Name = "BoxMSI4";
            this.BoxMSI4.Size = new System.Drawing.Size(35, 128);
            this.BoxMSI4.TabIndex = 29;
            this.BoxMSI4.TabStop = false;
            this.BoxMSI4.Visible = false;
            // 
            // WCKD
            // 
            this.WCKD.BackColor = System.Drawing.Color.Transparent;
            this.WCKD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.WCKD.Image = global::WCKD_The_Virus.Properties.Resources._74730ffcb962d9994a17ea2ee19d7a3a3;
            this.WCKD.Location = new System.Drawing.Point(221, 353);
            this.WCKD.Margin = new System.Windows.Forms.Padding(0);
            this.WCKD.Name = "WCKD";
            this.WCKD.Size = new System.Drawing.Size(28, 26);
            this.WCKD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.WCKD.TabIndex = 27;
            this.WCKD.TabStop = false;
            // 
            // BorderLeft
            // 
            this.BorderLeft.BackColor = System.Drawing.Color.Yellow;
            this.BorderLeft.Location = new System.Drawing.Point(9, 10);
            this.BorderLeft.Name = "BorderLeft";
            this.BorderLeft.Size = new System.Drawing.Size(41, 751);
            this.BorderLeft.TabIndex = 25;
            this.BorderLeft.TabStop = false;
            this.BorderLeft.Visible = false;
            // 
            // BorderBottom
            // 
            this.BorderBottom.BackColor = System.Drawing.Color.Yellow;
            this.BorderBottom.Location = new System.Drawing.Point(12, 752);
            this.BorderBottom.Name = "BorderBottom";
            this.BorderBottom.Size = new System.Drawing.Size(631, 10);
            this.BorderBottom.TabIndex = 24;
            this.BorderBottom.TabStop = false;
            this.BorderBottom.Visible = false;
            // 
            // BorderRight
            // 
            this.BorderRight.BackColor = System.Drawing.Color.Yellow;
            this.BorderRight.Location = new System.Drawing.Point(632, 10);
            this.BorderRight.Name = "BorderRight";
            this.BorderRight.Size = new System.Drawing.Size(10, 752);
            this.BorderRight.TabIndex = 23;
            this.BorderRight.TabStop = false;
            this.BorderRight.Visible = false;
            // 
            // BorderTop
            // 
            this.BorderTop.BackColor = System.Drawing.Color.Yellow;
            this.BorderTop.Location = new System.Drawing.Point(12, 10);
            this.BorderTop.Name = "BorderTop";
            this.BorderTop.Size = new System.Drawing.Size(631, 10);
            this.BorderTop.TabIndex = 22;
            this.BorderTop.TabStop = false;
            this.BorderTop.Visible = false;
            // 
            // BoxPCI1
            // 
            this.BoxPCI1.BackColor = System.Drawing.Color.Red;
            this.BoxPCI1.Location = new System.Drawing.Point(153, 408);
            this.BoxPCI1.Name = "BoxPCI1";
            this.BoxPCI1.Size = new System.Drawing.Size(46, 14);
            this.BoxPCI1.TabIndex = 20;
            this.BoxPCI1.TabStop = false;
            this.BoxPCI1.Visible = false;
            // 
            // BoxTransistor2
            // 
            this.BoxTransistor2.BackColor = System.Drawing.Color.Red;
            this.BoxTransistor2.Location = new System.Drawing.Point(268, 314);
            this.BoxTransistor2.Name = "BoxTransistor2";
            this.BoxTransistor2.Size = new System.Drawing.Size(153, 24);
            this.BoxTransistor2.TabIndex = 19;
            this.BoxTransistor2.TabStop = false;
            this.BoxTransistor2.Visible = false;
            // 
            // BoxMSI
            // 
            this.BoxMSI.BackColor = System.Drawing.Color.Red;
            this.BoxMSI.Location = new System.Drawing.Point(437, 496);
            this.BoxMSI.Name = "BoxMSI";
            this.BoxMSI.Size = new System.Drawing.Size(95, 119);
            this.BoxMSI.TabIndex = 18;
            this.BoxMSI.TabStop = false;
            this.BoxMSI.Visible = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Red;
            this.pictureBox15.Location = new System.Drawing.Point(221, 22);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(184, 48);
            this.pictureBox15.TabIndex = 17;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Visible = false;
            // 
            // BoxTransistor6
            // 
            this.BoxTransistor6.BackColor = System.Drawing.Color.Red;
            this.BoxTransistor6.Location = new System.Drawing.Point(116, 90);
            this.BoxTransistor6.Name = "BoxTransistor6";
            this.BoxTransistor6.Size = new System.Drawing.Size(50, 248);
            this.BoxTransistor6.TabIndex = 16;
            this.BoxTransistor6.TabStop = false;
            this.BoxTransistor6.Visible = false;
            // 
            // BoxTransistor5
            // 
            this.BoxTransistor5.BackColor = System.Drawing.Color.Red;
            this.BoxTransistor5.Location = new System.Drawing.Point(73, 647);
            this.BoxTransistor5.Name = "BoxTransistor5";
            this.BoxTransistor5.Size = new System.Drawing.Size(37, 32);
            this.BoxTransistor5.TabIndex = 15;
            this.BoxTransistor5.TabStop = false;
            this.BoxTransistor5.Visible = false;
            // 
            // BoxPCI5
            // 
            this.BoxPCI5.BackColor = System.Drawing.Color.Red;
            this.BoxPCI5.Location = new System.Drawing.Point(148, 658);
            this.BoxPCI5.Name = "BoxPCI5";
            this.BoxPCI5.Size = new System.Drawing.Size(51, 10);
            this.BoxPCI5.TabIndex = 14;
            this.BoxPCI5.TabStop = false;
            this.BoxPCI5.Visible = false;
            // 
            // BoxPCI3
            // 
            this.BoxPCI3.BackColor = System.Drawing.Color.Red;
            this.BoxPCI3.Location = new System.Drawing.Point(153, 556);
            this.BoxPCI3.Name = "BoxPCI3";
            this.BoxPCI3.Size = new System.Drawing.Size(46, 13);
            this.BoxPCI3.TabIndex = 13;
            this.BoxPCI3.TabStop = false;
            this.BoxPCI3.Visible = false;
            // 
            // BoxTransistor3
            // 
            this.BoxTransistor3.BackColor = System.Drawing.Color.Red;
            this.BoxTransistor3.Location = new System.Drawing.Point(182, 118);
            this.BoxTransistor3.Name = "BoxTransistor3";
            this.BoxTransistor3.Size = new System.Drawing.Size(17, 174);
            this.BoxTransistor3.TabIndex = 12;
            this.BoxTransistor3.TabStop = false;
            this.BoxTransistor3.Visible = false;
            // 
            // BoxTransistor1
            // 
            this.BoxTransistor1.BackColor = System.Drawing.Color.Red;
            this.BoxTransistor1.Location = new System.Drawing.Point(273, 118);
            this.BoxTransistor1.Name = "BoxTransistor1";
            this.BoxTransistor1.Size = new System.Drawing.Size(147, 20);
            this.BoxTransistor1.TabIndex = 11;
            this.BoxTransistor1.TabStop = false;
            this.BoxTransistor1.Visible = false;
            // 
            // BoxPCI6
            // 
            this.BoxPCI6.BackColor = System.Drawing.Color.Red;
            this.BoxPCI6.Location = new System.Drawing.Point(150, 706);
            this.BoxPCI6.Name = "BoxPCI6";
            this.BoxPCI6.Size = new System.Drawing.Size(232, 10);
            this.BoxPCI6.TabIndex = 10;
            this.BoxPCI6.TabStop = false;
            this.BoxPCI6.Visible = false;
            // 
            // BoxPCI4
            // 
            this.BoxPCI4.BackColor = System.Drawing.Color.Red;
            this.BoxPCI4.Location = new System.Drawing.Point(150, 613);
            this.BoxPCI4.Name = "BoxPCI4";
            this.BoxPCI4.Size = new System.Drawing.Size(213, 10);
            this.BoxPCI4.TabIndex = 9;
            this.BoxPCI4.TabStop = false;
            this.BoxPCI4.Visible = false;
            // 
            // BoxSSD
            // 
            this.BoxSSD.BackColor = System.Drawing.Color.Red;
            this.BoxSSD.Location = new System.Drawing.Point(227, 496);
            this.BoxSSD.Name = "BoxSSD";
            this.BoxSSD.Size = new System.Drawing.Size(194, 40);
            this.BoxSSD.TabIndex = 7;
            this.BoxSSD.TabStop = false;
            this.BoxSSD.Visible = false;
            // 
            // BoxPCI2
            // 
            this.BoxPCI2.BackColor = System.Drawing.Color.Red;
            this.BoxPCI2.Location = new System.Drawing.Point(150, 462);
            this.BoxPCI2.Name = "BoxPCI2";
            this.BoxPCI2.Size = new System.Drawing.Size(246, 14);
            this.BoxPCI2.TabIndex = 6;
            this.BoxPCI2.TabStop = false;
            this.BoxPCI2.Visible = false;
            // 
            // BorderLeft3
            // 
            this.BorderLeft3.BackColor = System.Drawing.Color.Red;
            this.BorderLeft3.Location = new System.Drawing.Point(47, 22);
            this.BorderLeft3.Name = "BorderLeft3";
            this.BorderLeft3.Size = new System.Drawing.Size(63, 400);
            this.BorderLeft3.TabIndex = 5;
            this.BorderLeft3.TabStop = false;
            this.BorderLeft3.Visible = false;
            // 
            // BoxCPU
            // 
            this.BoxCPU.BackColor = System.Drawing.Color.Red;
            this.BoxCPU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxCPU.Location = new System.Drawing.Point(308, 177);
            this.BoxCPU.Name = "BoxCPU";
            this.BoxCPU.Size = new System.Drawing.Size(97, 97);
            this.BoxCPU.TabIndex = 4;
            this.BoxCPU.TabStop = false;
            this.BoxCPU.Visible = false;
            // 
            // BoxRam
            // 
            this.BoxRam.BackColor = System.Drawing.Color.Red;
            this.BoxRam.Location = new System.Drawing.Point(474, 47);
            this.BoxRam.Name = "BoxRam";
            this.BoxRam.Size = new System.Drawing.Size(75, 345);
            this.BoxRam.TabIndex = 2;
            this.BoxRam.TabStop = false;
            this.BoxRam.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // MotherBoard1
            // 
            this.MotherBoard1.BackColor = System.Drawing.Color.Black;
            this.MotherBoard1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MotherBoard1.Image = global::WCKD_The_Virus.Properties.Resources.MotherBoard_12;
            this.MotherBoard1.Location = new System.Drawing.Point(9, 10);
            this.MotherBoard1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MotherBoard1.Name = "MotherBoard1";
            this.MotherBoard1.Size = new System.Drawing.Size(634, 752);
            this.MotherBoard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MotherBoard1.TabIndex = 0;
            this.MotherBoard1.TabStop = false;
            // 
            // MarginBox1
            // 
            this.MarginBox1.Location = new System.Drawing.Point(0, 0);
            this.MarginBox1.Name = "MarginBox1";
            this.MarginBox1.Size = new System.Drawing.Size(100, 50);
            this.MarginBox1.TabIndex = 0;
            this.MarginBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(654, 798);
            this.Controls.Add(this.finish);
            this.Controls.Add(this.p25);
            this.Controls.Add(this.e8);
            this.Controls.Add(this.e3);
            this.Controls.Add(this.e4);
            this.Controls.Add(this.e2);
            this.Controls.Add(this.e6);
            this.Controls.Add(this.e5);
            this.Controls.Add(this.e7);
            this.Controls.Add(this.e1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.p23);
            this.Controls.Add(this.p24);
            this.Controls.Add(this.p22);
            this.Controls.Add(this.p20);
            this.Controls.Add(this.p21);
            this.Controls.Add(this.p19);
            this.Controls.Add(this.p18);
            this.Controls.Add(this.p16);
            this.Controls.Add(this.p17);
            this.Controls.Add(this.p14);
            this.Controls.Add(this.p15);
            this.Controls.Add(this.p13);
            this.Controls.Add(this.p12);
            this.Controls.Add(this.p11);
            this.Controls.Add(this.p10);
            this.Controls.Add(this.p9);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p1);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.timpRamas);
            this.Controls.Add(this.PointsText);
            this.Controls.Add(this.Points);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Life);
            this.Controls.Add(this.BoxMSI7);
            this.Controls.Add(this.BoxMSI2);
            this.Controls.Add(this.BoxMSI5);
            this.Controls.Add(this.BoxMSI6);
            this.Controls.Add(this.BoxMSI3);
            this.Controls.Add(this.BoxMSI4);
            this.Controls.Add(this.WCKD);
            this.Controls.Add(this.BorderLeft);
            this.Controls.Add(this.BorderBottom);
            this.Controls.Add(this.BorderRight);
            this.Controls.Add(this.BorderTop);
            this.Controls.Add(this.BoxPCI1);
            this.Controls.Add(this.BoxTransistor2);
            this.Controls.Add(this.BoxMSI);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.BoxTransistor6);
            this.Controls.Add(this.BoxTransistor5);
            this.Controls.Add(this.BoxPCI5);
            this.Controls.Add(this.BoxPCI3);
            this.Controls.Add(this.BoxTransistor3);
            this.Controls.Add(this.BoxTransistor1);
            this.Controls.Add(this.BoxPCI6);
            this.Controls.Add(this.BoxPCI4);
            this.Controls.Add(this.BoxSSD);
            this.Controls.Add(this.BoxPCI2);
            this.Controls.Add(this.BorderLeft3);
            this.Controls.Add(this.BoxCPU);
            this.Controls.Add(this.BoxRam);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.MotherBoard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "MSI Map";
            ((System.ComponentModel.ISupportInitialize)(this.finish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxMSI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxTransistor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxSSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxPCI2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderLeft3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxCPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxRam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotherBoard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MarginBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox MotherBoard1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox MarginBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox BoxRam;
        private System.Windows.Forms.PictureBox BoxCPU;
        private System.Windows.Forms.PictureBox BorderLeft3;
        private System.Windows.Forms.PictureBox BoxPCI2;
        private System.Windows.Forms.PictureBox BoxSSD;
        private System.Windows.Forms.PictureBox BoxPCI4;
        private System.Windows.Forms.PictureBox BoxPCI6;
        private System.Windows.Forms.PictureBox BoxTransistor1;
        private System.Windows.Forms.PictureBox BoxTransistor3;
        private System.Windows.Forms.PictureBox BoxPCI3;
        private System.Windows.Forms.PictureBox BoxPCI5;
        private System.Windows.Forms.PictureBox BoxTransistor5;
        private System.Windows.Forms.PictureBox BoxTransistor6;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox BoxMSI;
        private System.Windows.Forms.PictureBox BoxTransistor2;
        private System.Windows.Forms.PictureBox BoxPCI1;
        private System.Windows.Forms.PictureBox BorderTop;
        private System.Windows.Forms.PictureBox BorderRight;
        private System.Windows.Forms.PictureBox BorderBottom;
        private System.Windows.Forms.PictureBox BorderLeft;
        private System.Windows.Forms.PictureBox WCKD;
        private System.Windows.Forms.PictureBox BoxMSI4;
        private System.Windows.Forms.PictureBox BoxMSI3;
        private System.Windows.Forms.PictureBox BoxMSI6;
        private System.Windows.Forms.PictureBox BoxMSI5;
        private System.Windows.Forms.PictureBox BoxMSI2;
        private System.Windows.Forms.PictureBox BoxMSI7;
        private System.Windows.Forms.ProgressBar Life;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar Points;
        private System.Windows.Forms.Label PointsText;
        private System.Windows.Forms.ProgressBar timpRamas;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox p7;
        private System.Windows.Forms.PictureBox p6;
        private System.Windows.Forms.PictureBox p5;
        private System.Windows.Forms.PictureBox p4;
        private System.Windows.Forms.PictureBox p3;
        private System.Windows.Forms.PictureBox p2;
        private System.Windows.Forms.PictureBox p1;
        private System.Windows.Forms.PictureBox p8;
        private System.Windows.Forms.PictureBox p9;
        private System.Windows.Forms.PictureBox p10;
        private System.Windows.Forms.PictureBox p11;
        private System.Windows.Forms.PictureBox p12;
        private System.Windows.Forms.PictureBox p13;
        private System.Windows.Forms.PictureBox p15;
        private System.Windows.Forms.PictureBox p14;
        private System.Windows.Forms.PictureBox p17;
        private System.Windows.Forms.PictureBox p16;
        private System.Windows.Forms.PictureBox p18;
        private System.Windows.Forms.PictureBox p19;
        private System.Windows.Forms.PictureBox p21;
        private System.Windows.Forms.PictureBox p20;
        private System.Windows.Forms.PictureBox p22;
        private System.Windows.Forms.PictureBox p24;
        private System.Windows.Forms.PictureBox p23;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox e1;
        private System.Windows.Forms.PictureBox e7;
        private System.Windows.Forms.PictureBox e5;
        private System.Windows.Forms.PictureBox e6;
        private System.Windows.Forms.PictureBox e2;
        private System.Windows.Forms.PictureBox e4;
        private System.Windows.Forms.PictureBox e3;
        private System.Windows.Forms.PictureBox e8;
        private System.Windows.Forms.PictureBox p25;
        private System.Windows.Forms.PictureBox finish;

        public object Properties { get; private set; }
        public object Graphics { get; private set; }
    }
}

