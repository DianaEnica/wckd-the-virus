﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WCKD_The_Virus
{
    public partial class Maps : Form
    {
        public Maps()
        {
            InitializeComponent();
        }

        private void MSI_map_mouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\button.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                this.Hide();
                Form1 open_game_form = new Form1();
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
        }

        private void Back_Button_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\button.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                this.Hide();
                MainMenu open_game_menu = new MainMenu();
                open_game_menu.Closed += (s, args) => this.Close();
                open_game_menu.Show();
            }
            
        }

        private void ASUS_map_mouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\button.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                this.Hide();
                Form2 open_game_form = new Form2();
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
        }
    }
}
